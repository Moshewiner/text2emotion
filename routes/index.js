var express = require('express');
var router = express.Router();
var request = require('request');
var tones = {};
var _ = require('lodash');


/* GET home page. */
router.get('/getEmotionFromText', function(req, res) {
  tones.request = req.query.text;
  request.post(
      'https://tone-analyzer-demo.mybluemix.net/api/tone',
      { json: { text: tones.request } },
      function (error, response, data) {
          tones.response = data.document_tone.tone_categories[0];
          tones.max = _.maxBy(tones.response.tones, (tone)=> tone.score);
          res.status(200).json(tones.max.tone_name);
      }
  );

});

module.exports = router;
